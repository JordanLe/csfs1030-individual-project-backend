/***Table 1 -- Users**/
CREATE TABLE IF NOT EXISTS user (
    userID VARCHAR(255) NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL DEFAULT '',
    email VARCHAR(255) NOT NULL DEFAULT '',
    password VARCHAR(255) NOT NULL DEFAULT ''
    );

/***Table 1 -- Users**/
INSERT INTO user VALUES
("eaa46d24-db94-4717-8f2d-dedb0aa56008", "John Doe", "johndoe@gmail.com", "$2b$10$XzAKPIdOPilrrsuDd3pdLO9syohjO1JDmZCJfFzICBnhysv6QPVIi"),
("8797f1db-71f5-49eb-a86c-a2f9b1efdd1a", "Jordan Lee", "Jordan@Lee.com", "$2b$10$YUK7kRNTWMd0aGdpLVM4buQf3Oj0Omi34.dulWxMSRHP.h6B.RzxO")


/**Table 2 -- Contact Entries**/
CREATE TABLE IF NOT EXISTS contactentries (
    entryID VARCHAR(255) NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL DEFAULT '',
    email VARCHAR(255) NOT NULL DEFAULT '',
    phoneNumber VARCHAR(11) NOT NULL DEFAULT '',
    content MEDIUMTEXT NOT NULL
    );				

/**Table 2 -- Contact Entries**/
INSERT INTO contactentries VALUES
("287e3da1-c4b2-4ec3-ae18-39bdd5903f3a","Tom Hanks","tom@hanks.com","1231231231","blah blah blah"),
("ec3726a0-69b7-49ed-95b2-e976490f4d02","John Cena","John@Cena.com","7894561231","blah blah blah")


/*** Table 3 -  Portfolio**/
CREATE TABLE IF NOT EXISTS projects (
    projectID INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    picture VARCHAR(255) NOT NULL,
    title VARCHAR(255) NOT NULL DEFAULT '',
    description MEDIUMTEXT NOT NULL
    );


/*** Table 3 -  Portfolio**/
INSERT INTO projects VALUES
(1,"/images/website-image1.JPG", "Nature Gallery" , "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."),
(2,"/images/website-image2.png", "Rosa Restaurant" , "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."),
(3,"/images/website-image3.JPG", "Health Connect" , "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.")
